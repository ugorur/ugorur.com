---
layout: page
title: Hakkımda
permalink: /hakkimda/
---

Yaklaşık 15 yıldır kodlama ile iç içeyim. PHP, Ruby, Python, Node, C/C++, Shell ve diğer dillerde çeşitli projeler ile para kazandım. En sevdiğim dil Ruby. En çok kullandığım dil PHP. Bilgisayar dillerini insan dillerinden iyi biliyorum. boş zamanlarında da elektronik ile uğraşıyorum.

10 yılı aşkın süredir Linux kullanıcısıyım. Development makinamda Ubuntu çekirdeği üztüne i3wm kurulu. Ayrıca TV ve Medya makinamda ElementaryOS sunucularımda da Centos ve Ubuntu Server kurulumları mevcut. Docker ile çalışmayı seviyorum.
