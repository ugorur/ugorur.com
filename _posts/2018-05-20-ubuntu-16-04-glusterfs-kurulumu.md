---
layout: post
title:  "Ubuntu 16.04 GlusterFS Kurulumu"
date:   2018-05-20 05:35:57 +0300
category: devops
excerpt_separator: <!--more-->
tags: [ ubuntu, glusterfs ]
---

Ubuntu Servar'lar da GlusterFS ile çok sunuculu dizin yaratmak için kurulumu, gerekli kodları ve komutları her serferinde dağınık kaynaklardan almamak için kendime not olarak bırakıyorum.
<!--more-->

Öncelikle gerekli uygulamarı kuralım ve sistemi güncelleyelim. Aşağıdaki adım tüm sunucularda uygulanması gerekiyor.

{% highlight bash %}
sudo add-apt-repository -y ppa:gluster/glusterfs-4.0
sudo apt update && sudo apt upgrade -y
sudo apt install -y glusterfs-server
{% endhighlight %}

> 21 May 2018 - GlusterFS Kurulum adımında yer alan **sudo** sorunu çözüldü.

Sunucunun Gluster için kullanacağınız ip adresini bir değişkene yazalım. 

{% highlight bash %}
PRIVATE_IP=192.168.7.15 #Buraya sunucunun private ip adresini (yada aynı private ağda değilse public ip adresini) yazın.
{% endhighlight %}

Eğer Digitalocean kullanıyorsanız aşağıdaki komut ile bu işlemi yapabilirsiniz.

{% highlight bash %}
PRIVATE_IP=$(curl -s http://169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address)
{% endhighlight %}

Hosts dosyamıza kullanacağımız diğer sunucuların ip adreslerini ekleyelim. Bunlarda bize yardımcı olacaktır. Bunları tüm sunucularda yapabilirsiniz ama zorunlu değil. Eğer tüm sunucularda yapacaksanız, $PRIVATE_IP değişkenini doğru yere taşımanız gerekiyor. Buradaki IP Adreslerini kendize göre düzenlemeyi unutmayın.

{% highlight bash %}
echo "$PRIVATE_IP server1" | sudo tee -a /etc/hosts
echo "192.168.7.29 server2" | sudo tee -a /etc/hosts
echo "192.168.7.30 server3" | sudo tee -a /etc/hosts
{% endhighlight %}

Artık GlusterFS'yi başlatma zamanımız geldi. Öncelikle tüm sunucuları GlusterFS'ye tanıtmamız gerekiyor. Aşağıdaki adımları sadece bir sunucuda yapmanız yeterli.

{% highlight bash %}
gluster peer probe server2
gluster peer probe server3
{% endhighlight %}

Şimdi yeni bir GlusterFS sürücüsü oluşturalım. Burda kullanacağınız dizin yolu dosyaları hangi dizinde bulundurmak istediğinize göre ayarlamanız gerekmektedir. Ben harici bir diski /gluster dizinine bağlayıp onu kullanmayı seviyorum. Burda tercih tamamen size bağlı. Buradaki her sürücü kendi başına network disk gibi davranabilecektir. Sürücü adlarını ve sayısını istediğiniz gibi ayarlayabilirsiniz.

{% highlight bash %}
gluster volume create driver1 replica 3 transport tcp server2:/gluster/driver1 server3:/gluster/driver1 $PRIVATE_IP:/gluster/driver1 force
gluster volmue start driver1

gluster volume create driver2 replica 3 transport tcp server2:/gluster/driver2 server3:/gluster/driver2 $PRIVATE_IP:/gluster/driver2 force
gluster volmue start driver2
{% endhighlight %}

Sıra sürücülerimizi kullanmaya geldi. Bu sürücüleri tüm sunucularada kullanacağımız dizinlere bağlamamız gerekiyor. Aynı zamanda dizinleri fstab dosyamıza da ekleyerek sistem yeniden başlattıkca sürücülerimiz dizinlere otomatik bağlanma sağlayacak.

{% highlight bash %}
mount -t glusterfs $PRIVATE_IP:/driver1 /var/www/demosite.com
mount -t glusterfs $PRIVATE_IP:/driver2 /var/www/examplesite.com

echo "$PRIVATE_IP:/drive1 /var/www/demosite.com glusterfs defaults,_netdev 0 0" | sudo tee -a /etc/fstab
echo "$PRIVATE_IP:/drive2 /var/www/examplesite.com glusterfs defaults,_netdev 0 0" | sudo tee -a /etc/fstab
{% endhighlight %}

Eğer sunuculardan birinde bu dizinlere dosya eklerseniz, diğerlerinde de o dosyayı görebilirsiniz.
