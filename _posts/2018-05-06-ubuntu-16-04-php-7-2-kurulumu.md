---
layout: post
title:  "Ubuntu 16.04 PHP 7.2 Kurulumu"
date:   2018-05-06 04:40:10 +0300
category: devops
excerpt_separator: <!--more-->
tags: [ php, ubuntu, apache ]
---

Ubuntu Server'lar da Apache ile birlikte PHP 7.2 kurulumunu, gerekli kodları ve komutları her seferinde dağınık kaynaklardan almamak için kendime not olarak bırakıyorum.
<!--more-->

Öncelikle sisteme gerekli uygulamaları kuralım ve sistemi güncelleyelim. Bu yüklemeler PHP ve Postgres kullanılacaksa geçerlidir. Başka database kullanılacaksa ona göre düzenleme gerekiyor.

{% highlight bash %}
sudo add-apt-repository ppa:ondrej/php -y
sudo apt update && sudo apt upgrade -y
sudo apt install -y apache2 php7.2 libapache2-mod-php7.2 php7.2-mbstring php7.2-pgsql php7.2-curl php7.2-gd php-pear php7.2-pspell php7.2-recode php7.2-snmp php7.2-bz2 php7.2-dev php7.2-zip php7.2-tidy php7.2-xmlrpc php7.2-xsl php7.2-json php7.2-xdebug
{% endhighlight %}

> 21 May 2018 - PHP Kurulum adımında yer alan **sudo** sorunu ve eksik **xdebug** sorunu çözüldü.

### Composer Kurulumu

PHP paket yöneticisi Composer kuruyoruz

{% highlight bash %}
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php --install-dir=/usr/bin --filename=composer
rm composer-setup.php
{% endhighlight %}

### Apache Ayarları

Apache için varsayılan hostname olarak localhost ekliyoruz

{% highlight bash %}
echo "ServerName localhost" | sudo tee -a /etc/apache2/apache2.conf > /dev/null
{% endhighlight %}

.htaccess rewrite ve header için gerekli Apache eklentisini açıyoruz

{% highlight bash %}
sudo systemctl stop apache2.service
sudo a2enmod rewrite
sudo a2enmod headers
sudo systemctl start apache2.service
{% endhighlight %}

### Kullandığım php.ini Ayarları

Sistelerimde kullandığım ve tek server yapılandırması içinde kullanılabilecek php.ini ayarları:

{% highlight ini %}
[PHP]
engine = On
short_open_tag = Off
precision = 14
output_buffering = 4096
zlib.output_compression = Off
implicit_flush = Off
unserialize_callback_func =
serialize_precision = 17
disable_functions = pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority
disable_classes =
zend.enable_gc = On
expose_php = Off

max_execution_time = 600

max_input_time = 600

memory_limit = 512M

error_reporting = E_ALL

display_errors = Off

display_startup_errors = Off

log_errors = On

log_errors_max_len = 1024
ignore_repeated_errors = On
ignore_repeated_source = Off
report_memleaks = On
track_errors = Off
html_errors = On
variables_order = "GPCS"
request_order = "GP"
register_argc_argv = Off
auto_globals_jit = On
post_max_size = 512M

auto_prepend_file =
auto_append_file =

default_mimetype = "text/html"
default_charset = "UTF-8"
doc_root =
user_dir =
enable_dl = Off
file_uploads = On

upload_max_filesize = 2048M

max_file_uploads = 20

allow_url_fopen = On
allow_url_include = Off
default_socket_timeout = 60

[CLI Server]
cli_server.color = On

[Date]
date.timezone = Europe/Istanbul

[SQL]
sql.safe_mode = Off

[Interbase]
ibase.allow_persistent = 1
ibase.max_persistent = -1
ibase.max_links = -1
ibase.timestampformat = "%Y-%m-%d %H:%M:%S"
ibase.dateformat = "%Y-%m-%d"
ibase.timeformat = "%H:%M:%S"

[PostgreSQL]
pgsql.allow_persistent = On
pgsql.auto_reset_persistent = Off
pgsql.max_persistent = -1
pgsql.max_links = -1
pgsql.ignore_notice = 0
pgsql.log_notice = 0

[bcmath]
bcmath.scale = 0

[Session]
session.save_handler = files
session.use_strict_mode = 0
session.use_cookies = 1
session.use_only_cookies = 1
session.name = FMREACTOR
session.auto_start = 0
session.cookie_lifetime = 0
session.cookie_path = /
session.cookie_domain =
session.cookie_httponly =
session.serialize_handler = php
session.gc_probability = 0
session.gc_divisor = 1000

session.gc_maxlifetime = 3600

session.referer_check =
session.cache_limiter = nocache
session.cache_expire = 180
session.use_trans_sid = 0
session.hash_function = 0
session.hash_bits_per_character = 5
url_rewriter.tags = "a=href,area=href,frame=src,input=src,form=fakeentry"

[Assertion]
zend.assertions = -1
{% endhighlight %}
