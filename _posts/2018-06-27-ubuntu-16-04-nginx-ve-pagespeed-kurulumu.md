---
layout: post
title:  "Ubuntu 16.04 Nginx ve Pagespeed Kurulumu"
date:   2018-06-27 13:30:08 +0300
category: devops
excerpt_separator: <!--more-->
tags: [ ubuntu, nginx, pagespeed ]
---

Ubuntu Servar'lar da Nginx ve Pagespeed kurulumu için gerekli kodları ve komutları her serferinde dağınık kaynaklardan almamak için kendime not olarak bırakıyorum.
<!--more-->

Öncelikle gerekli uygulamarı kuralım ve sistemi güncelleyelim. Aşağıdaki adım tüm sunucularda uygulanması gerekiyor.

{% highlight bash %}
sudo apt update && sudo apt upgrade -y
sudo apt install -y build-essential zlib1g-dev libpcre3 libpcre3-dev unzip uuid-dev
{% endhighlight %}

Sistemde geçici kurulum dosyaları ile çalışabilmek için */tmp* dizini içinde *nginx* diye bir dizin açalım ve orada ya geçiş yapalım.

{% highlight bash %}
cd /tmp
mkdir nginx
cd nginx
{% endhighlight %}

Şimdi gerekli değişkenleri tanımlayalım. Bu yazıyı yazdığım sırada bu değişkenler en güncel stabil versiyonları temsil ediyordu. Bunları en yenileri ile değiştirmek gerekiyor. 
Nginx için: [http://nginx.org/en/download.html](http://nginx.org/en/download.html)
Pagespeed için: [https://www.modpagespeed.com/doc/release_notes](https://www.modpagespeed.com/doc/release_notes)

{% highlight bash %}
NGINX_VERSION=1.15.0 #Nginx Versiyonu
NPS_VERSION=1.13.35.2-stable #Pagespeed Versiyonu
{% endhighlight %}

Öncelikle Pagespeed için gereken dosyaların indirilmesi ve gerekli ayarların yapılması lazım. Bunun için aşağıdaki komutları kullanabiliriz. Bu komutları kendi sitesinden aldım. Adres: [https://www.modpagespeed.com/doc/build_ngx_pagespeed_from_source](https://www.modpagespeed.com/doc/build_ngx_pagespeed_from_source)

{% highlight bash %}
wget https://github.com/apache/incubator-pagespeed-ngx/archive/v${NPS_VERSION}.zip
unzip v${NPS_VERSION}.zip
nps_dir=$(find . -name "*pagespeed-ngx-${NPS_VERSION}" -type d)
cd "$nps_dir"
NPS_RELEASE_NUMBER=${NPS_VERSION/beta/}
NPS_RELEASE_NUMBER=${NPS_VERSION/stable/}
psol_url=https://dl.google.com/dl/page-speed/psol/${NPS_RELEASE_NUMBER}.tar.gz
[ -e scripts/format_binary_url.sh ] && psol_url=$(scripts/format_binary_url.sh PSOL_BINARY_URL)
wget ${psol_url}
tar -xzvf $(basename ${psol_url})
# Pagespeed indirlidikten sonra Google'dan pagespeed için gerekli PSOL indiriliyor*
{% endhighlight %}

Şimdi Nginx'in kurulumunu gerçekleştire bililiriz. Bunun için önce OpenSSL kütüphanesini indiriyoruz.

{% highlight bash %}
cd ..
wget https://www.openssl.org/source/openssl-1.1.0f.tar.gz
tar -xzvf openssl-1.1.0f.tar.gz
{% endhighlight %}

Nginx'i indirip configure edelim ve kurulumu tamamlayalım.

{% highlight bash %}
wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz
tar -xvzf nginx-${NGINX_VERSION}.tar.gz
cd nginx-${NGINX_VERSION}/

./configure --add-module=/tmp/nginx/$nps_dir --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-openssl=/tmp/nginx/openssl-1.1.0f

make
sudo make install
{% endhighlight %}

Nginx için gerekli son ayarları yapalım ve sistemi test edelim.

{% highlight bash %}
sudo ln -s /usr/lib64/nginx/modules /etc/nginx/modules
sudo useradd --system --home /var/cache/nginx --shell /sbin/nologin --comment "nginx user" --user-group nginx
sudo mkdir -p /var/cache/nginx/client_temp
nginx -t
{% endhighlight %}

Teknik olarak Nginx kurulumu tamalandı ama henüz bir servis olarak system.d içine tanımlı değil. Bununu eklememiz gerekiyor. Bunun için */etc/systemd/system/nginx.service* dosyasını açarak içine aşağıda yer alan servis bilgilerini ekleyelim.


{% highlight ini %}
[Unit]
Description=nginx - high performance web server
Documentation=https://nginx.org/en/docs/
After=network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/var/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t -c /etc/nginx/nginx.conf
ExecStart=/usr/sbin/nginx -c /etc/nginx/nginx.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s TERM $MAINPID

[Install]
WantedBy=multi-user.target
{% endhighlight %}

Kaydettikten sonra servisimizi aktif edelim ve başlatalım.

{% highlight bash %}
sudo systemctl start nginx.service 
sudo systemctl enable nginx.service
{% endhighlight %}

Nginx'in Pagespeed ile kurulumu tamamlanmış oldu. Bundan sonrası için başka yazıda kendime notlar bırakacağım.
