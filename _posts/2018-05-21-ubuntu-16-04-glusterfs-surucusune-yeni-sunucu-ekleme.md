---
layout: post
title:  "Ubuntu 16.04 GlusterFS Sürücümüze Yeni Sunucu Eklemek"
redirect_from:
  - /devops/2018-05-21-ubuntu16-04-glusterfs-surucusune-yeni-sunucu-ekleme/
date:   2018-05-21 10:47:05 +0300
category: devops
excerpt_separator: <!--more-->
tags: [ ubuntu, glusterfs ]
---

Ubuntu Servar'lar da GlusterFS sürücülerimize yeni bir sunucu eklemek için gerekli kodları ve komutları her serferinde dağınık kaynaklardan almamak için kendime not olarak bırakıyorum.
<!--more-->

Sunucunun Gluster için kullanacağınız ip adresini bir değişkene yazalım. 

{% highlight bash %}
PRIVATE_IP=192.168.8.123 #Buraya sunucunun private ip adresini (yada aynı private ağda değilse public ip adresini) yazın.
{% endhighlight %}

Eğer Digitalocean kullanıyorsanız aşağıdaki komut ile bu işlemi yapabilirsiniz.

{% highlight bash %}
PRIVATE_IP=$(curl -s http://169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address)
{% endhighlight %}

GlusterFS kurulumu yapılan sunucunun hosts dosyasına yeni sunucumuzu ekleyelim. Hosts dosyasına ekleme yapmak, gelecekti işlerimizde kolaylık sağlayacaktır.

{% highlight bash %}
echo "192.168.8.123 server4" | sudo tee -a /etc/hosts
{% endhighlight %}

Yeni sunucumuzu GlusterFS'ye tanıtalım.

{% highlight bash %}
gluster peer probe server4
{% endhighlight %}

Tanıttığımız sunucuyu eski sürücümüze ekleyelim.

{% highlight bash %}
gluster volume add-brick driver1 replica 4 server4:/gluster/driver1 force
{% endhighlight %}

Sıra sürücümüzü kullanmaya geldi. Bu sürücüyü yeni sunucuda kullanacağımız dizine bağlamamız gerekiyor. Aynı zamanda dizini fstab dosyamıza da ekleyerek sistem yeniden başlattıkca sürücümüzü dizine otomatik bağlanma sağlayacak.

{% highlight bash %}
mount -t glusterfs $PRIVATE_IP:/driver1 /var/www/demosite.com

echo "$PRIVATE_IP:/drive1 /var/www/demosite.com glusterfs defaults,_netdev 0 0" | sudo tee -a /etc/fstab
{% endhighlight %}

Eğer sunuculardan birinde bu dizinlere dosya eklerseniz, diğerlerinde de o dosyayı görebilirsiniz.
