---
layout: post
title:  "VIM ile PHP IDE Notlarım"
date:   2018-06-07 07:43:10 +0300
category: development
excerpt_separator: <!--more-->
tags: [ php, vim ]
---

Vim ile PHP geliştirme yapabilmek için gerekli olan eklentiler ve kısayollarım için kendime bıraktığım notlar.
<!--more-->

Bu ayarlara başlamadan önce [bu](http://web-techno.net/vim-php-ide/) sayfadan kendime referans aldım. Bu adresi de kendime not olarak bırakıyorum.

## Refactor

### Kullandığım eklenti
[https://github.com/adoy/vim-php-refactoring-toolbox](https://github.com/adoy/vim-php-refactoring-toolbox)

### Kısayollar
{% highlight vim %}
nnoremap <unique> <Leader>rlv :call PhpRenameLocalVariable()<CR>
nnoremap <unique> <Leader>rcv :call PhpRenameClassVariable()<CR>
nnoremap <unique> <Leader>rm :call PhpRenameMethod()<CR>
nnoremap <unique> <Leader>eu :call PhpExtractUse()<CR>
vnoremap <unique> <Leader>ec :call PhpExtractConst()<CR>
nnoremap <unique> <Leader>ep :call PhpExtractClassProperty()<CR>
vnoremap <unique> <Leader>em :call PhpExtractMethod()<CR>
nnoremap <unique> <Leader>np :call PhpCreateProperty()<CR>
nnoremap <unique> <Leader>du :call PhpDetectUnusedUseStatements()<CR>
vnoremap <unique> <Leader>== :call PhpAlignAssigns()<CR>
nnoremap <unique> <Leader>sg :call PhpCreateSettersAndGetters()<CR>
nnoremap <unique> <Leader>cog :call PhpCreateGetters()<CR>
nnoremap <unique> <Leader>da :call PhpDocAll()<CR>
{% endhighlight %}

Devam Edecek...
